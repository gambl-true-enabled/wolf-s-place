package com.wolfs.place.net

import com.wolfs.place.net.RawData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface WebApi {
    @GET("check/")
    suspend fun check(@Header("appsflyer_id") uid: String): Response<RawData>
}